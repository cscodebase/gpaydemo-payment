1、demo.handler.PayHandler用来向gpay发送请求，对签名算法有疑问的可以看看这个类的sign方法。
2、demo的入口是 demo.server.Server 。
3、demo.server.Server运行起来之后，在浏览器访问： http://127.0.0.1:8088/static-pay/pay.html 。
4、gpay相关的配置，在 demo.config.Config 类中。 
5、demo.handler.PayNotificationHandler 用来处理来自gpay的支付通知。