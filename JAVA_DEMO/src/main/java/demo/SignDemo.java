package demo;

import demo.config.Config;
import demo.hanlder.PayHandler;
import demo.lib.Utils;

import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SignDemo {


    public static String sign(PayHandler.PayContext ctx, Map<String, String> requestParams, String key)
            throws NoSuchAlgorithmException {
        String plain = Utils.md5(key) + requestParams.get("company_id") + requestParams.get("bank_id")
                + requestParams.get("amount") + requestParams.get("company_order_num")
                + requestParams.get("company_user") + requestParams.get("estimated_payment_bank")
                + requestParams.get("deposit_mode") + requestParams.get("group_id") + requestParams.get("web_url")
                + requestParams.get("memo") + requestParams.get("note_model");
        String sign = Utils.md5(plain);

        ctx.content_to_sign = plain;
        ctx.sign = sign;

        return sign;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        Map<String, String> requestParams = new HashMap<>();

        requestParams.put("company_id", "C0000001");
        requestParams.put("bank_id", "");
        requestParams.put("amount", "1000");
        requestParams.put("company_order_num", "ORD" + new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date()));
        requestParams.put("company_user", "merchant_user_id01");
        requestParams.put("estimated_payment_bank", "");
        requestParams.put("deposit_mode", "deposit_mode34");
        requestParams.put("group_id", "");
        requestParams.put("web_url", "https://www.baidu.com");
        requestParams.put("memo", "");
        requestParams.put("note", "");
        requestParams.put("note_model", "");
        requestParams.put("terminal", "web");
        requestParams.put("client_ip", "127.0.0.1");
        PayHandler.PayContext ctx = new PayHandler.PayContext();
        requestParams.put("key", SignDemo.sign(ctx,
                requestParams,
                Config.gpay_merchant_key  // 《==这个需要改
        ));

        System.out.println("签名串:" + ctx.content_to_sign);

        System.out.println("签名结果:" + ctx.sign);


    }
}
