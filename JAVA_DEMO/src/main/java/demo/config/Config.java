package demo.config;

public class Config {
	/**
	 * gpay支付api域名。
	 */
	public static final String gpay_api_domain = "https://gapi.llque.com";
	/**
	 * gpay商户密钥，也就是在商户后台看到的PSK。
	 */
	public static final String gpay_merchant_key = "";
}
