package demo.hanlder;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class BaseHandler implements HttpHandler {
	String urlPrefix;

	public String getUrlPrefix() {
		return urlPrefix;
	}

	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix;
	}
	
	protected void sendResponse(HttpExchange exchange, int code, byte[] data) throws IOException {
		exchange.sendResponseHeaders(code, data.length);
		exchange.getResponseBody().write(data);
		exchange.getResponseBody().close();
	}
}
