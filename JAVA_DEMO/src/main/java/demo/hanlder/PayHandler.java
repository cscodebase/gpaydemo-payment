package demo.hanlder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import demo.config.Config;
import demo.lib.Utils;

/**
 * 处理demo页面的支付请求。
 *
 */
public class PayHandler extends BaseHandler implements HttpHandler {
	Logger logger = LoggerFactory.getLogger(getClass());

	public static class PayContext {
		public String content_to_sign = ""; // 计算支付请求动态密钥的明文。等于：
		// MD5(config)+company_id+bank_id+amount+company_order_num+company_user+estimated_payment_bank+deposit_mode+group_id+web_url+memo+note+note_model
		// 。
		public String sign = ""; // 支付请求的签名。
		public String request_message = ""; // 支付请求消息。
		public String response_message = ""; // 支付应答消息。
		public String break_url = ""; // 支付应答中的支付链接： break_url 。
	}

	/**
	 * 计算签名。
	 * 动态密钥：MD5(MD5(config)+company_id+bank_id+amount+company_order_num+company_user+estimated_payment_bank+deposit_mode+group_id+web_url+memo+note+note_model)
	 * 备注：config为静态密码。
	 * 
	 * @param params
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	protected String sign(PayContext ctx, Map<String, String> requestParams, String key)
			throws NoSuchAlgorithmException {
		String plain = Utils.md5(key) + requestParams.get("company_id") + requestParams.get("bank_id")
				+ requestParams.get("amount") + requestParams.get("company_order_num")
				+ requestParams.get("company_user") + requestParams.get("estimated_payment_bank")
				+ requestParams.get("deposit_mode") + requestParams.get("group_id") + requestParams.get("web_url")
				+ requestParams.get("memo") + requestParams.get("note_model");
		String sign = Utils.md5(plain);

		ctx.content_to_sign = plain;
		ctx.sign = sign;

		return sign;
	}

	/**
	 * 构造支付请求。
	 * 
	 * @param ctx
	 * @param params
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	protected Map<String, String> makeRequestParams(PayContext ctx, Map<String, String> params)
			throws NoSuchAlgorithmException {
		Map<String, String> requestParams = new HashMap<>();

		requestParams.put("company_id", params.get("company_id"));
		requestParams.put("bank_id", "");
		requestParams.put("amount", params.get("amount"));
		requestParams.put("company_order_num", "ORD" + new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date()));
		requestParams.put("company_user", params.get("merchant_user_id"));
		requestParams.put("estimated_payment_bank", "");
		requestParams.put("deposit_mode", params.get("deposit_mode"));
		requestParams.put("group_id", "");
		requestParams.put("web_url", "https://www.baidu.com");
		requestParams.put("memo", "");
		requestParams.put("note", "");
		requestParams.put("note_model", "");
		requestParams.put("terminal", params.get("terminal"));
		requestParams.put("client_ip", params.getOrDefault("client_ip", "127.0.0.1"));
		requestParams.put("key", this.sign(ctx, requestParams, Config.gpay_merchant_key));

		return requestParams;
	}

	/**
	 * 向gpay发送支付请求。
	 * 
	 * @param ctx
	 * @param requestParams
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	protected Map<String, String> doPayRequest(PayContext ctx, Map<String, String> requestParams)
			throws ClientProtocolException, IOException {
		String requestData = Utils.encode(requestParams);
		String responseData = org.apache.http.client.fluent.Request.Post(Config.gpay_api_domain + "/api/v2/mownecum/pay_request")
				.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
				.bodyString(requestData, ContentType.create("application/x-www-form-urlencoded", Consts.UTF_8))
				.connectTimeout(10000).socketTimeout(10000).execute().returnContent().asString();

		ctx.request_message = requestData;
		ctx.response_message = responseData;

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(responseData, new TypeReference<HashMap<String, String>>() {});
	}

	/**
	 * 请求支付。
	 * 
	 * @param params
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	protected PayContext requestPay(Map<String, String> params)
			throws NoSuchAlgorithmException, ClientProtocolException, IOException {
		PayContext ctx = new PayContext();
		Map<String, String> requestParams = this.makeRequestParams(ctx, params);
		Map<String, String> responseParams = this.doPayRequest(ctx, requestParams);
		
		ctx.break_url = responseParams.getOrDefault("break_url", "");
		return ctx;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestBody = IOUtils.toString(exchange.getRequestBody(), StandardCharsets.UTF_8);
		Map<String, String> params = Utils.decode(requestBody);
		try {
			PayContext ctx = this.requestPay(params);
			exchange.getResponseHeaders().add("Content-Type", "application/json");
			exchange.getResponseHeaders().add("Content-Type", "application/json");
			this.sendResponse(exchange, 200, new ObjectMapper().writeValueAsBytes(ctx));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.sendResponse(exchange, 200, "failed request pay".getBytes());
	}

}
