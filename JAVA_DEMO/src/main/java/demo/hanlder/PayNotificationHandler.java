package demo.hanlder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import demo.config.Config;
import demo.lib.Utils;

/**
 * 处理来自gpay的支付通知。 目前，仅是打印日志、校验签名。 支付通知是gpay主动向商户发起请求。
 */
public class PayNotificationHandler extends BaseHandler implements HttpHandler {
	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 这是发给gpay的应答消息。
	 *
	 */
	public static class NotificaitonResponse {
		public String company_order_num;
		public String mownecum_order_num;
		public int status;
		public String error_msg;
	}

	/**
	 * 计算签名。
	 * 
	 * 动态密钥:
	 * MD5(MD5(config)+pay_time+bank_id+amount+company_order_num+mownecum_order_num+pay_card_num+pay_card_name+channel+area+fee+transaction_charge+deposit_mode)
	 * 备注：Config为静态密码。
	 * 
	 * 
	 * @param params
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	protected String sign(Map<String, String> requestParams, String key) throws NoSuchAlgorithmException {
		String plain = Utils.md5(key) + requestParams.get("pay_time") + requestParams.get("bank_id")
				+ requestParams.get("amount") + requestParams.get("company_order_num")
				+ requestParams.get("mownecum_order_num") + requestParams.get("pay_card_num")
				+ requestParams.get("pay_card_name") + requestParams.get("channel") + requestParams.get("area")
				+ requestParams.get("fee") + requestParams.get("transaction_charge")
				+ requestParams.get("deposit_mode");
		String sign = Utils.md5(plain);

		return sign;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String requestBody = IOUtils.toString(exchange.getRequestBody(), StandardCharsets.UTF_8);
		logger.info("notification received: {}", requestBody);

		Map<String, String> params = Utils.decode(requestBody);
		NotificaitonResponse response = new NotificaitonResponse();
		response.company_order_num = params.getOrDefault("company_order_num", "");
		response.mownecum_order_num = params.getOrDefault("mownecum_order_num", "");

		String sign;
		try {
			sign = this.sign(params, Config.gpay_merchant_key);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			response.status = 0;
			response.error_msg = "internal error";
			this.sendResponse(exchange, 200, new ObjectMapper().writeValueAsBytes(response));
			return;
		}
		
		String signReceived = params.getOrDefault("key", "");

		if (!sign.equals(signReceived)) {
			logger.error("sign not matched, sign: {}, sign received: {}", sign, signReceived);
			response.status = 0; // 失败
			response.error_msg = "failed validate signature";
		} else {
			response.status = 1; // 成功
			response.error_msg = "";
		}

		this.sendResponse(exchange, 200, new ObjectMapper().writeValueAsBytes(response));
	}
}
