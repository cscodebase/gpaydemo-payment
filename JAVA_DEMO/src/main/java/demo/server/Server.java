package demo.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.*;

import demo.hanlder.BaseHandler;
import demo.hanlder.PayHandler;
import demo.hanlder.PayNotificationHandler;
import demo.hanlder.StaticFileHandler;

public class Server {
	int listenPort = 8088;
	HttpServer httpServer;
	Logger logger = LoggerFactory.getLogger(getClass());

	protected void buildServer() throws IOException {
		InetSocketAddress addr = new InetSocketAddress("0.0.0.0", listenPort);
		this.httpServer = HttpServer.create(addr, 100);
	}
	
	protected void registerHandlers(HttpServer httpServer) {
		this.registerHandler(httpServer, "/pay", new PayHandler());
		this.registerHandler(httpServer, "/notification", new PayNotificationHandler());
		this.registerHandler(httpServer, "/static-js/", new StaticFileHandler("./src/main/webapp/js"));
		this.registerHandler(httpServer, "/static-pay/", new StaticFileHandler("./src/main/webapp/pay"));
	}
	
	protected void registerHandler(HttpServer httpServer, String urlPrefix, BaseHandler httpHandler) {
		httpServer.createContext(urlPrefix, httpHandler);
		httpHandler.setUrlPrefix(urlPrefix);
	}

	public void start() {
		try {
			this.buildServer();
			this.registerHandlers(this.httpServer);
			this.httpServer.start();
			logger.info("server is running ...");
		} catch (Exception e) {
			logger.error("failed start, ", e);
		}
	}

	public static void main(String[] args) {
		Server server = new Server();
		server.start();
	}
}
