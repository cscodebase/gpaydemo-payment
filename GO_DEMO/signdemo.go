package main

import "fmt"

import "encoding/hex"
import "crypto/md5"


func main() {
    sign_demo()
}

func md5str(origin string) string {
    ph :=  md5.Sum([]byte(origin));
    p := hex.EncodeToString(ph[:])
    return p;
}

func sign(params map[string]string , psk string) {
    p := md5str(psk);

    origin :=  p + params["company_id"] + params["bank_id"]  + params["amount"] + params["company_order_num"] + params["company_user"] + params["estimated_payment_bank"] + params["deposit_mode"] + params["group_id"] + params["web_url"] + params["memo"] + params["note_model"];
    
    sign_text := md5str(origin);
    fmt.Println("签名串:" + origin);
    fmt.Println("签名结果:" + sign_text);
    
}
func sign_demo() {
    params := map[string]string {
        "sf": "df",
        
        "company_id": "C0000001",
        "bank_id": "",
        "amount": "1000",
        "company_order_num": "ORD20201001091759",
        "company_user": "merchant_user_id01",
        "estimated_payment_bank": "",
        "deposit_mode": "deposit_mode34",
        "group_id": "",
        "web_url": "https://www.baidu.com",
        "memo": "",
        "note": "",
        "note_model": "",
        "terminal": "web",
        "client_ip": "127.0.0.1",
    }
    
    //第二个参数是gpay商户密钥，也就是在商户后台看到的PSK。一定要改。
    sign(params,"Lorem ipsum dolor sit amet");
    
}