<?php

class gxn
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $api = 'https://gapi.llque.com/api/v2/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($company_id, $config)
    {
        $this->company_id = $company_id;
        $this->config = $config;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function pay_request($amount, $company_order_num, $company_user, $deposit_mode, $group_id, $web_url, $terminal, $client_ip)
    {
        $key = md5(md5($this->config) . $this->company_id . $amount . $company_order_num . $company_user . $deposit_mode . $group_id . $web_url);
        $parameters = array(
            'company_id' => $this->company_id,
            'amount' => $amount,
            'company_order_num' => $company_order_num,
            'company_user' => $company_user,
            'deposit_mode' => $deposit_mode,
            'group_id' => $group_id,
            'web_url' => $web_url,
            'key' => $key,
            'terminal' => $terminal,
            'client_ip' => $client_ip
        );
        $url = $this->api . 'mownecum/pay_request';
        $result=$this->post($url,http_build_query($parameters));
        //{"bank_card_num":"","bank_acc_name":"","amount":"0.10","email":"","company_order_num":"test001","datetime":"","note":"","mownecum_order_num":"a5f694cd-0232-4fb3-a1ee-24c37c98e780","status":"1","error_msg":"","mode":"","issuing_bank_address":"","break_url":"https://gpay.llque.com/ui/pay?id=a5f694cd-0232-4fb3-a1ee-24c37c98e780","deposit_mode":"38","collection_bank_id":"","collection_bank_name":"","deposit_page":"","key":"0cb0e2bdd392caa78d76013863d2518d"}
        if(!empty($result)){
            $r=json_decode($result);
            if($r->status==1){
                return $r->break_url;
            }else{
                //出错
            }
        }else{
            return 'error';
        }
    }

    public function pay_query($company_order_num){
        $key=md5(md5($this->config).$this->company_id.$company_order_num);
        $parameters = array(
            'company_id' => $this->company_id,
            'company_order_num' => $company_order_num,
            'key' => $key
        );
        $url=$this->api.'mownecum/pay_query';
        return $this->post($url,http_build_query($parameters));
    }
    public static function post($url, $post_data = '', $timeout = 5){//curl
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        if($post_data != ''){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }
}
